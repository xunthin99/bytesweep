from setuptools import setup, find_packages

setup(
	name='bytesweep',
	version='1.0',
	packages=['bytesweep'],
	scripts=['scripts/bytesweep'],
)
